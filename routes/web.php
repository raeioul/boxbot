<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Preview route
|--------------------------------------------------------------------------
|
| Route for prismic.io preview functionality
|
*/

Route::get('/preview', function (Request $request) {
    $token = $request->input('token');
    if (!isset($token)) {
        return abort(400, 'Bad Request');
    }
    $url = $request->attributes->get('api')->previewSession($token, $request->attributes->get('linkResolver'), '/');
    return response(null, 302)->header('Location', $url);
});

/*
|--------------------------------------------------------------------------
| Index route
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return redirect('/home');
});


/*
|--------------------------------------------------------------------------
| 404 Page Not Found
|--------------------------------------------------------------------------
*/
Route::get('/home', function (Request $request) {
    // Query the API
    
    $document = $request->attributes->get('api')->getSingle('homepage');
    
    //$document = $api->getSingle('home');
    
    //$document = $request->attributes->get('api')->getByUID('home', $uid);
    // Display the 404 page if no document is found
    if (!$document) {
        return view('404');
    }

    // Render the page
    return view('home', ['document' => $document]);
});

Route::get('/contact', function (Request $request) {
    $document = $request->attributes->get('api')->getSingle('contact');
    if (!$document) {
        return view('404');
    }
    return view('contact', ['document' => $document]);
});

Route::get('/careers', function (Request $request) {
    $document = $request->attributes->get('api')->getSingle('career');
    if (!$document) {
        return view('404');
    }
    return view('careers', ['document' => $document]);
});

Route::get('/partners', function (Request $request) {
    $document = $request->attributes->get('api')->getSingle('partners');
    if (!$document) {
        return view('404');
    }
    return view('partners', ['document' => $document]);
});

Route::get('/{path}', function () {
    return view('404');
});

Route::get('/page/{uid}', function ($uid, Request $request) {
    // Query the API
    $document = $request->attributes->get('api')->getByUID('pages', $uid);
    
    // Display the 404 page if no document is found
    if (!$document) {
        return view('404');
    }

    // Render the page
    return view('page', ['document' => $document]);
});

