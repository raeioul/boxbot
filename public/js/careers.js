var url = "https://api.lever.co/v0/postings/boxbot?mode=json"

var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "GET",
    "data": "{\n\t\"operationName\":null",
}

$.ajax(settings).done(function(response) {
    let locations = new Set();
    let teams = new Set();
    let workType = new Set();

    response.forEach((val) => {
        locations.add(val.categories.location);
        teams.add(val.categories.team);
        workType.add(val.categories.commitment);
    });

    locations.forEach((location) => {
        let locationsFilter = document.querySelector('#locations .dropdown-menu');
        let locationItem = document.createElement('a');
        locationItem.className = 'dropdown-item';
        locationItem.textContent = location;
        locationItem.addEventListener('click', function() {
            url = settings.url = "https://api.lever.co/v0/postings/boxbot?location=" + location + "&mode=json";
            getData();
        })

        locationsFilter.appendChild(locationItem);
    })

    teams.forEach((team) => {
        let teamsFilter = document.querySelector('#teams .dropdown-menu');
        let teamItem = document.createElement('a');
        teamItem.className = 'dropdown-item';
        teamItem.textContent = team;
        teamItem.addEventListener('click', function() {
            url = settings.url = "https://api.lever.co/v0/postings/boxbot?team=" + team + "&mode=json";
            getData();
        })

        teamsFilter.appendChild(teamItem);
    })

    workType.forEach((type) => {
        let typesFilter = document.querySelector('#workTypes .dropdown-menu');
        let typeItem = document.createElement('a');
        typeItem.className = 'dropdown-item';
        typeItem.textContent = type;
        typeItem.addEventListener('click', function() {
            url = settings.url = "https://api.lever.co/v0/postings/boxbot?commitment=" + type + "&mode=json";
            getData();
        })

        typesFilter.appendChild(typeItem);
    })
})

function getData() {
    let jobListEl = document.querySelector('.jobs-list');
    jobListEl.innerHTML = '';
    $.ajax(settings).done(function(response) {
        console.log(response);

        response.forEach((val) => {

            let rowEl = document.createElement('div');
            rowEl.className = 'row justify-content-between';

            let columnTitleEl = document.createElement('div');
            columnTitleEl.className = 'col-lg-6';

            let titleEl = document.createElement('h6');
            titleEl.textContent = val.text;

            columnTitleEl.appendChild(titleEl);

            rowEl.appendChild(columnTitleEl);

            let columnButton = document.createElement('div');
            columnButton.className = 'col-lg-6 right';

            let buttonEl = document.createElement('button');
            buttonEl.className = 'ButtonApplyStyle'
            buttonEl.textContent = 'APPLY';
            buttonEl.addEventListener('click', function() {
                window.location = val.applyUrl
            });



            columnButton.appendChild(buttonEl);

            rowEl.appendChild(columnButton);

            jobListEl.appendChild(rowEl);
            jobListEl.appendChild(document.createElement('br'));
        })
    });
}


getData();