@php
    use Prismic\Dom\RichText;
@endphp

@extends('layouts.app')

@section('content')

<div class="row box no-gutters" style="
    background-image: url('{{$document->data->contactdesktop->url}}'); 
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;">
        <div class="col">
            <div class="row align no-gutters">
                <div class="We-want-to-hear-from">
                    <p>{{ RichText::asText($document->data->title1) }}</p>
                    <p class="Subtitle">{{ RichText::asText($document->data->subtitle1) }}</p>
                </div>
            </div>

            <div class="row align no-gutters mobile-no-borders">
                <div class="col-8 bg-white rounded">
                    <div class="row p-4">
                        <div class="col-lg-3 border border-white contact-no-borders">
                            <p class="titleCol">{{ RichText::asText($document->data->titlemail1) }}</p>
                            <p class="subtitleCol">
                                <a href="mailto:{{ RichText::asText($document->data->mail1) }}">{{ RichText::asText($document->data->mail1) }}</a>
                            </p>
                        </div>
                        <div class="col-lg-3 border border-bottom-0 border-top-0 border-danger contact-no-borders">
                            <p class="titleCol">{{ RichText::asText($document->data->titlemail2) }}</p>
                            <p class="subtitleCol">
                                <a href="mailto:{{ RichText::asText($document->data->mail2) }}">{{ RichText::asText($document->data->mail2) }}</a>
                            </p>
                        </div>
                        <div class="col-lg-3 border border-bottom-0 border-top-0 border-left-0 border-danger contact-no-borders">
                            <p class="titleCol">{{ RichText::asText($document->data->titlemail3) }}</p>
                            <p class="subtitleCol">
                                <a href="mailto:{{ RichText::asText($document->data->mail1) }}">{{ RichText::asText($document->data->mail3) }}</a>
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <p class="titleCol">{{ RichText::asText($document->data->titlemail4) }}</p>
                            <p class="subtitleCol">
                                <a href="mailto:{{ RichText::asText($document->data->mail1) }}">{{ RichText::asText($document->data->mail4) }}</a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <br></div>

    </div>
    @include('layouts.footer')
@stop    
