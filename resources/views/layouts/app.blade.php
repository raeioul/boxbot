<!DOCTYPE html>
<html lang="en">
<head>
    {{--  Meta  --}}
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--  Meta Title --}}
    <title>BoxBot</title>

    {{--  Scripts  --}}
    <script>
        // Required for previews and experiments
        window.prismic = {
            endpoint: '{{ $endpoint }}'
        };
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
    <div class="wrapper">
        <div class="header">
            <nav>
                <div class="logo">
                <a href="/home" ><img src="assets/Logo.svg"/></a>
                </div>
                <div class="menu desktop">
                    <ul>
                        <li><a href="/partners" class="{{ Request::url() == url('partners') ? 'underlined' : '' }}">Partners</a></li>
                        <li><a href="/careers" class="{{ Request::url() == url('careers') ? 'underlined' : '' }}">Careers</a></li>
                        <li class="link-border"><a href="/contact" class="{{ Request::url() == url('contact') ? 'contact-underlined':'contact' }}">Contact</a></li>
                    </ul>
                </div>
                <div class="menu-icon mobile-bar-open">
                    <i class="fa fa-bars fa-2x"></i>
                </div>
            </nav>
        </div>
        
        <main>
            @yield('content')
        </main>
    </div>
    <div class="menu-mobile close">
        <div class="overlay">
            <div class="mobile-header">
                <div>
                    <a href="/home" ><img src="assets/boxbot.png"/></a>
                </div>
                <div class="mobile-bar-close">
                    <i class="fa fa-bars fa-2x"></i>
                </div>
            </div>
            <div class="mobile-links">
                <ul>
                    <li class="m-border"><a href="/partners" >Partners</a></li>
                    <li class="m-border"><a href="/careers" >Careers</a></li>
                    <li class="m-border-contact"><a href="/contact" class="contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('.mobile-bar-open').click(function() {
            $('.wrapper').addClass('close');
            $('.menu-mobile').addClass('open');
            $('.menu-mobile').removeClass('close');
        });
        $('.mobile-bar-close').click(function() {
            $('.wrapper').removeClass('close');
            $('.menu-mobile').removeClass('open');
            $('.menu-mobile').addClass('close');
        });
    });
    // $(document).ready(function(){
    //     $(".contact").hover(function(){
    //         $(this).removeClass('contact');
    //         $(this).addClass("link-border");
    //     });
    //     $('.contact').on('mouseleave', function() {
    //         $(this).removeClass('link-border');
    //         $(this).addClass("contact");
    //     });
    // });
    // $(document).ready(function() {
    //     $(".menu-icon").on("click", function() {
    //         $("nav ul").toggleClass("showing");
    //     });
    // });
        // Scrolling Effect
    $(window).on("scroll", function() {
        if($(window).scrollTop()) {
            $('nav').addClass('black');
        }
        else {
            $('nav').removeClass('black');
        }
    })
</script>