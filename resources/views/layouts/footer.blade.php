@php
    use Prismic\Api;
    use Prismic\Dom\RichText;
    $prismic= Config::get('prismic.url');
    $api = Api::get($prismic);
    $document = $api->getSingle('footer');
@endphp
<footer>
    <div class="contact-us no-gutters">
        <div class="contact-us-container">
            <p class="Stay-Up-To-Date">{!! RichText::asText($document->data->stayuptodate) !!}</p>
            <br>
            <p class="Get-the-latest-news">{!! RichText::asText($document->data->latestnews) !!}</p>
            <div class="input-group mb-3" style="margin: auto; width: unset;">
                <div id="mc_embed_signup">
                    <form action="https://itangelo.us1.list-manage.com/subscribe/post?u=ba9263ef5f013424ab21b638b&amp;id=5c0dd9e9f8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">    
                            <div class="mc-field-group">
                                <div>
                                    <input type="email" value="" placeholder="Your email address" aria-label="Your email address" name="EMAIL" class="required email" id="mce-EMAIL">
                                </div>
                                <input class="subscribe" type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                            </div>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ba9263ef5f013424ab21b638b_5c0dd9e9f8" tabindex="-1" value=""></div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper footer menu footer-navigation d-none d-lg-block">
        <div class="header footer-header">
            <nav >
                <div class="logo">
                    <a href="/home" ><img src="assets/Logo2.svg" height="37px" width="160px"/></a>
                </div>
                <div class="menu">
                    <ul >
                        <li><a href="/partners" >Partners</a></li>
                        <li><a href="/careers" >Careers</a></li>
                        <li class="link-border"><a href="/contact" class="contact">Contact</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>    
</footer>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->