@php
    use Prismic\Dom\RichText;
@endphp

@extends('layouts.app')

@section('content')
<div class="home">
  <div class="hero">
    <div style="background-image: url(assets/Hero-Map-Desktop.svg); background-size: 100%;">
        <div class="container description">
            <div class="col justify-content-center align-self-center text-1">
                <div class="first-text-style">{!! RichText::asHtml($document->data->title1) !!}</div>
                <div class="secondary-text-style">{!! RichText::asHtml($document->data->subtitle1) !!}</div>
                <a href="#contact-sales" ><button class="button-center ">Contact Sales</button></a>
            </div>
        </div>
    </div>
    <div class="partners-image"></div>
  </div>
  
  <div class="info fixed partners-background">
        <div class="consumers-steps">
            <div>
                <div>
                    <div>
                        <h4><p>{!! RichText::asText($document->data->subtitle21) !!}</p></h4>
                    </div>
                    <div class="orange-line2">
                        <p>{!! RichText::asText($document->data->comment21) !!}</p>
                    </div>    
                </div>
                <div>
                    <h4><p>{!! RichText::asText($document->data->subtitle22) !!}</p></h4>
                    <div class="orange-line2">
                        <p>{!! RichText::asText($document->data->comment22) !!}</p>
                    </div>
                </div>
                <div>
                    <h4><p>{!! RichText::asText($document->data->subtitle23) !!}</p></h4>
                    <div class="orange-line2 final">
                        <p>{!! RichText::asText($document->data->comment23) !!}</p>
                    </div>    
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="card mx-auto border-0" style="width: 100%;max-width: 1100px;">
                <div class="card-body backgroundBox">
                    <div class="row no-gutters mx-auto">
                        <p class="Last-mile-System-Sta mx-auto"><span class="text-dark">Last-mile</span> System Stack</p>
                    </div>
                    <div class="d-flex flex-lg-row flex-wrap justify-content-around">
                        <p class="line">{!! RichText::asText($document->data->subtitle31) !!}</p>
                        <img src="assets/Icon-Warehouse-With-Oval.svg">
                        <ul>
                          <li class="list">{!! RichText::asText($document->data->item31) !!}</li>
                          <li class="list">{!! RichText::asText($document->data->item32) !!}</li>
                        </ul>
                    </div>
                    <br>
                    <div class="d-flex flex-lg-row flex-wrap justify-content-around">
                        <p class="line">{!! RichText::asText($document->data->subtitle32) !!}</p>
                        <img src="assets/Icon-Truck-With-Oval.svg">
                        <ul>
                        <li class="list">{!! RichText::asText($document->data->item311) !!}</li>
                                <li class="list">{!! RichText::asText($document->data->item32) !!}</li>
                            </ul>
                    
                    </div>
                    <br>
                    <div class="d-flex flex-lg-row flex-wrap justify-content-around">
                        <p class="line">{!! RichText::asText($document->data->subtitle33) !!}</p>
                        <img src="assets/Icon-Dots-With-Oval.svg">
                        <ul>
                        <li class="list">{!! RichText::asText($document->data->item331) !!}</li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="join-us">
          <div class="backgroundBox2 d-flex flex-column flex-lg-row flex-xl-row flex-md-row flex-fill justify-content-center align-items-center p-3">
            <div class="partner-with-us partner-with-us-text d-flex align-items-md-center align-items-sm-center">
                <div class="card border-0 partner" style="background-color: transparent;">
                    <p class="Partner-with-us">{!! RichText::asText($document->data->title4) !!}</p>
                    <p class="list">{!! RichText::asText($document->data->subtitle4) !!}</p>
                </div>
            </div>
            <div class="partner-with-us d-flex" id="contact-sales">
                <div class="card">
                    <h5 class="cardText">Let’s deliver together</h5>
                    <form action="https://itangelo.us1.list-manage.com/subscribe/post" method="POST">
                      <input type="hidden" name="u" value="ba9263ef5f013424ab21b638b">
                      <input type="hidden" name="id" value="51e54aa739">
                      <div id="mergeTable" class="partner-form">
                          <div class="partner-form-field">
                            <input type="text" placeholder="Full Name" name="MERGE1" id="MERGE1" size="25" value="">
                            <input type="email" placeholder="Email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" >
                          </div>
                          <div class="partner-form-field full">
                            <input type="text" placeholder="Company Name" name="MERGE2" id="MERGE2" size="25" value="">
                          </div>
                          <div class="partner-form-field full">
                            <input type="text" placeholder="Tell us how we can work together" name="MERGE5" id="MERGE5" size="25" value="">
                          </div>
                      </div>
                      <div class="partner-form-submit">
                          <input type="submit" class="formEmailButton" name="submit" value="Contact Sales">
                      </div>
                    </form>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@stop