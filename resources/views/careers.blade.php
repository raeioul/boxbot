@php
    use Prismic\Dom\RichText;
@endphp

@extends('layouts.app')

@section('content')

<div class="home">
    <div class="hero">
        <div style="background-image: url(assets/Hero-Map-Desktop.svg); background-size: 100%;">
            <div class="container description">
                <div class="col justify-content-center align-self-center text-1">
                    <div class="first-text-style">{!! RichText::asText($document->data->title1) !!}</div>
                    <div class="secondary-text-style">
                        {!! RichText::asText($document->data->subtitle1) !!}
                        {{ RichText::asText($document->data->subtitle2) }}
                    </div>
                </div>
            </div>
        </div>
        <div style="background-image: url('{{$document->data->careersdesktop->url}}'); background-size: 100%;">
        </div>
    </div>
    <div class="row no-gutters" style="background-color: #f9f9f9">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <div class="card border-0">
                <div class="card-header border-0">
                    <div class="row-12">
                        FILTER BY:
                        <div class="btn-group col-2" id="locations">
                            <button style="background-color:#eeebebde" class="btn btn-dark btn-sm dropdown-toggle border-0 text-dark" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                CITY
                            </button>
                            <div class="dropdown-menu">

                            </div>
                        </div>
                        <div class="btn-group col-2" id="teams">
                            <button style="background-color:#eeebebde" class="btn btn-secondary btn-sm dropdown-toggle border-0 text-dark" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                TEAM
                            </button>
                            <div class="dropdown-menu">

                            </div>

                        </div>
                        <div class="btn-group col-2" id="workTypes">
                            <button style="background-color:#eeebebde" class="btn btn-secondary btn-sm dropdown-toggle border-0 text-dark" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                WORK TYPE
                            </button>
                            <div class="dropdown-menu">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body background" style="background-color: #f9f9f9">
                    <div class="jobs-list">
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-3"></div>
    </div>
    <br>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="careers.js"></script>
    
    </div>
</div>

<script src="js/careers.js"></script>
    @include('layouts.footer')
@stop    