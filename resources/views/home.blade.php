@php
    use Prismic\Dom\RichText;
@endphp

@extends('layouts.app')

@section('content')
<div class="home">
  <div class="hero">
    <div style="background-image: url(assets/Hero-Map-Desktop.svg); background-size: 100%;">
        <div class="container description">
            <div class="col justify-content-center align-self-center text-1">
                <div class="first-text-style">{!! RichText::asHtml($document->data->title1) !!}</div>
                <div class="secondary-text-style">{!! RichText::asHtml($document->data->subtitle1) !!}</div>
                <a href="/contact" ><button class="button-center ">Contact Boxbot</button></a>
            </div>
        </div>
    </div>
    <div class="home-image">
    </div>
  </div>
  <div class="highlight">
    <div class="logos">
        <p><img class="n-1" src="{{$document->data->imagecomment1->url}}"></p>
        <p><img class="n-2" src="{{$document->data->imagecomment2->url}}"></p>
        <p><img class="n-3" src="{{$document->data->imagecomment3->url}}"></p>
    </div>
    <div class="notes">
        <p class="description-logos">{{$document->data->comment1[0]->text}}</p>
        <p class="description-logos">{{$document->data->comment2[0]->text}}</p>
        <p class="description-logos">{{$document->data->comment3[0]->text}}</p>
    </div>
    <div class="logos mobile">
        <p><img class="n-1" src="{{$document->data->imagecomment1->url}}"></p>
        <p class="description-logos">{{$document->data->comment1[0]->text}}</p>
        <p><img class="n-2" src="{{$document->data->imagecomment2->url}}"></p>
        <p class="description-logos">{{$document->data->comment2[0]->text}}</p>
        <p><img class="n-3" src="{{$document->data->imagecomment3->url}}"></p>
        <p class="description-logos">{{$document->data->comment3[0]->text}}</p>
    </div>
  </div>
  <div class="info main-background">
        <h1 class="title2">{{ RichText::asText($document->data->title2) }}<span style="color: #FF594C">{{ RichText::asText($document->data->title22) }}</span></h1>
        <div class="info-table">
            <div>
                <p><img src="{{$document->data->icon21->url}}" ></p>
                <div class="orange-line">
                    <h4><p>{{ RichText::asText($document->data->titlecomment21) }}</p></h4>
                    <p>{{$document->data->comment21[0]->text}}</p>
                </div>    
            </div>
            <div>
                <p><img src="{{$document->data->icon22->url}}" ></p>
                <div class="orange-line">
                    <h4><p>{{ RichText::asText($document->data->titlecomment22) }}</p></h4>
                    <p>{{$document->data->comment22[0]->text}}</p>
                </div>
            </div>
            <div>
                <p><img src="{{$document->data->icon23->url}}" ></p>
                <div class="info-last">
                    <h4><p>{{ RichText::asText($document->data->titlecomment23) }}</p></h4>
                    <p>{{$document->data->comment23[0]->text}}</p>
                </div>    
            </div>
        </div>
        <div class="contact-sales">
            <div>    
                <div class="blue-left">
                    <div class="container d-flex h-100">
                        <div class="col justify-content-center align-self-center text-1">
                            <div class="first-text-style">{!! RichText::asHtml($document->data->title3) !!}</div>
                            <div class="secondary-text-style">{!! RichText::asHtml($document->data->subtitle3) !!}</div>
                            <a class="button-center" href="/partners#contact-sales">Contact Sales</a>
                        </div>
                    </div>
                </div>
                <div class="reliable-partner">
                    <img src="{{$document->data->image3->url}}"/>
                </div>
            </div>
        </div>
        <h1 class="title2 consumers">{{ RichText::asText($document->data->title4) }}<span style="color: #FF594C">{{ RichText::asText($document->data->title41) }}</span></h1>
        <div class="consumers-steps">
            <div>
                <div>
                    <div>
                        <h4><span class="step">1</span><p style="margin-left:15px">{{ RichText::asText($document->data->subtitle41) }}</p></h4>
                    </div>
                    <div class="orange-line2">
                        <p>{{$document->data->comment41[0]->text}}</p>
                    </div>    
                </div>
                <div>
                    <h4><span class="step">2</span><p style="margin-left:15px">{{ RichText::asText($document->data->subtitle42) }}</p></h4>
                    <div class="orange-line2">
                        <p>{{$document->data->comment42[0]->text}}</p>
                    </div>
                </div>
                <div>
                    <h4><span class="step">3</span><p style="margin-left:15px">{{ RichText::asText($document->data->subtitle43) }}</p></h4>
                    <div class="orange-line2 final">
                        <p>{{$document->data->comment43[0]->text}}</p>
                    </div>    
                </div>
            </div>
        </div>
        <div class="join-us" style="background-image: url(assets/Waves-Desktop.svg); background-size: 100%;">
            <div class="col justify-content-center align-self-center" style="text-align:center;">
                <p >
                    <video style="max-width:100%; height:auto" loop="loop" height='511px'width='911px' autoplay="autoplay" poster="img/poster.jpg" controls muted> 
                        <source src="video/website.webm" type="video/webm">
                        <source src="video/website.mp4" type="video/mp4">
                    </video>
                </p>
                <img src="{{$document->data->image4->url}}">
                <p><h1>{!! RichText::asHtml($document->data->title5) !!}</h1>
                <h3>{!! RichText::asHtml($document->data->subtitle5) !!}</h3>
                <h5>{!! RichText::asHtml($document->data->comment5) !!}</h5>
                <h5>{!! RichText::asHtml($document->data->comment51) !!}</h5></p>
                <br/>
                <a class="button-center border-0" href="/careers">Careers at Boxbot</a>
                <br/>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
@stop
